package com.company;

import java.awt.*;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random rnd = new Random();

        int n;
        int[] mas;

        System.out.print("input n: ");
        n = scanner.nextInt();

        mas = new int[n];

        for (int i=0;i<n;i++)
        {
            mas[i] = rnd.nextInt(20);
        }

        for (int i=0;i<n;i++)
        {
            System.out.print(mas[i]+" ");
        }
        System.out.println();

        //0)Usual search in unsorted array
        /*int findNumber, findIndex;

        System.out.print("input findNumber: ");
        findNumber = scanner.nextInt();

        findIndex=-1;
        for(int i=0; i<n; i++)
        {
            if(mas[i]==findNumber)
            {
                findIndex=i;
                break;
            }
        }

        System.out.println("findIndex : "+findIndex);*/

        //1) bin search in sorted array

        /*int findNumber, findIndex=-1;;
        int leftSide=0, rightSide=n;
        int middleSide;
        int countSpin=0;

        System.out.print("input findNumber: ");
        findNumber = scanner.nextInt();

        while (rightSide-leftSide>1)
        {
            countSpin++;
            middleSide=(rightSide+leftSide)/2;
            if (mas[middleSide]==findNumber)
            {
                findIndex = middleSide;
                break;
            }
            else if (mas[middleSide]<findNumber)
            {
                rightSide = middleSide;
            }
            else if (mas[middleSide]>findNumber)
            {
                leftSide = middleSide;
            }
        }
        System.out.println("findIndex : "+findIndex);
        System.out.println("countSpin : "+countSpin);*/

        //2)reverse
        /*
        int temp=0;
        for (int i=0;i<n/2;i++)
        {
            temp=mas[i];
            mas[i]=mas[n-1-i];
            mas[n-1-i]=temp;

        }*/
        //3)deletion

        /*
        int deleteIndex=2;
        for (int i=deleteIndex;i<n-1;i++)
        {
            mas[i]=mas[i+1];
        }
        n--;

        for (int i=0;i<n;i++)
        {
            System.out.print(mas[i]+" ");
        }
        System.out.println();*/

        //4)find repeat
        for (int i=0;i<n-1;i++)
        {
            for(int j=i+1; j<n; j++)
            {
                if(mas[i]==mas[j])
                {
                    System.out.println(mas[i]+" = "+ mas[j]+" "+i+" , "+j);
                }
            }
        }

    }
}
